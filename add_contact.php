<?php
// Получение данных из инпута
$name = $_POST['name'];
$phone = $_POST['phone'];

// Загрузка текущих контактов из файла JSON
$contacts = json_decode(file_get_contents('contacts.json'), true);

// метод делаеть уникального идентификатора для контакта
$id = uniqid();

// Создает новый контакт
$newContact = [
    'id' => $id,
    'name' => $name,
    'phone' => $phone
];

// Добавление нового контакта к текущему списку
$contacts[] = $newContact;

// Сохранение обновленного списка контактов в файл JSON
file_put_contents('contacts.json', json_encode($contacts, JSON_PRETTY_PRINT));

// Перенаправление на главную страницу
header('Location: index.php');
exit;
?>

