<!DOCTYPE html>
<html>
<head>
    <title>Телефонный справочник</title>
    <style>
        /* Минимальный CSS для стилизации */
        body { font-family: Arial, sans-serif; }
        form { margin-bottom: 20px; }
        .contact { margin-bottom: 10px; }
    </style>
</head>
<body>
    <h1>Телефонный справочник</h1>

    <form action="add_contact.php" method="post">
        <input type="text" name="name" placeholder="Имя" required>
        <input type="tel" name="phone" placeholder="Телефонный номер" required>
        <button type="submit">Добавить</button>
    </form>

    <h2>Список контактов</h2>
    <?php
        // Отображение списка контактов из файла JSON
        $contacts = json_decode(file_get_contents('contacts.json'), true);
        if ($contacts) {
            foreach ($contacts as $contact) {
                echo '<div class="contact">';
                echo '<strong>' . htmlspecialchars($contact['name']) . '</strong>: ' . htmlspecialchars($contact['phone']);
                echo ' <a href="delete_contact.php?id=' . $contact['id'] . '">Удалить</a>';
                echo '</div>';
            }
        } else {
            echo 'Список контактов пуст';
        }
    ?>
</body>
</html>

