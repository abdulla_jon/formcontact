<?php
// Получение идентификатора контакта для удаления
$id = $_GET['id'];

// Загрузка текущих контактов из файла JSON
$contacts = json_decode(file_get_contents('contacts.json'), true);

// Поиск контакта по идентификатору и его удаление из списка
foreach ($contacts as $key => $contact) {
    if ($contact['id'] == $id) {
        unset($contacts[$key]);
        break;
    }
}

// Сохранение обновленного списка контактов в файл JSON
file_put_contents('contacts.json', json_encode($contacts, JSON_PRETTY_PRINT));

header('Location: index.php');
exit;
?>

